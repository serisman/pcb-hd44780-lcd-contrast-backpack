# HD44780 LCD Contrast Backpack #

This module allows the use of common 5v LCDs with systems that have less than 5v (i.e. battery operated systems).  It can be mounted directly on top of the first three pins (1,2,3) of the LCD.

Populate U1 with a LM334M and VR1 with a 1k potentiometer.

For a >4v system, connect solder jumper J1.  You can leave the back unpopulated.

For a >2v system, connect solder jumper J2 (and leave solder jumper J1 empty). Populate U2 with a MAX660 (or ICL7660 compatible) IC and C1/C2 with 10uF capacitors.

The LM334 sinks a constant current (adjustable with VR1) from the LCD's VO pin (and hence provides constant contrast) regardless of VCC voltage (as long as VCC - GND > 4v, for >4v system config, or VCC > 2v for the >2v system config).

It is safe to use the >2v system config for a >4v system (i.e. a 5v system), but there will be more wasted current in the LM334.
## Schematic ##

![Schematic.png](https://bytebucket.org/serisman/pcb-hd44780-lcd-contrast-backpack/raw/master/output/Schematic.png)

## Design Rules ##

* Clearance: 8 mil
* Track Width: 20 mil
* Via Diameter: 27 mil
* Via Drill: 13 mil
* Zone Clearance: 8 mil
* Zone Min Width: 8 mil
* Zone Thermal Antipad Clearance: 8 mil
* Zone Thermal Spoke Width: 15 mil

## PCB available on OSH Park ##

* 0.52" x 0.34" (13.26 mm x 8.69 mm)
* $0.2833 each ($0.85 for 3)
* [https://oshpark.com/shared_projects/gGA5cmx3](https://oshpark.com/shared_projects/gGA5cmx3)

### PCB Front ###

![PCB-Front.png](https://bytebucket.org/serisman/pcb-hd44780-lcd-contrast-backpack/raw/master/output/PCB-Front.png)

### PCB Back ###

![PCB-Back.png](https://bytebucket.org/serisman/pcb-hd44780-lcd-contrast-backpack/raw/master/output/PCB-Back.png)